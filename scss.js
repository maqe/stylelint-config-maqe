module.exports = {
    "extends": ["./index.js", "./plugins/stylelint-scss.js"],
    rules: {
        // turn off when use scss
        "declaration-property-value-no-unknown": null,
        "media-query-no-invalid": null,
        "property-no-unknown": null,
    },
}