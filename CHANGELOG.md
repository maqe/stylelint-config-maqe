## v4.1.1 (2025-1-31):

- Following rules have been changed:
	- @stylistic/named-grid-areas-alignment
		- Change `alignQuotes` option to `false`

## v4.1.0 (2024-10-10):

- Bump stylelint version to v16.9.0
- Bump stylelint-scss version to v6.7.0
- Bump postcss version to v8.4.47
- Bump @stylistic/stylelint-plugin version to v3.1.1
- Following rules have been enabled:
	- @stylistic/named-grid-areas-alignment
	- scss/no-unused-private-members
	- scss/declaration-property-value-no-unknown
	- declaration-no-important
- Following rules have been changed:
	- declaration-block-no-redundant-longhand-properties
		- Add `inset`, `transition` in `ignoreShorthands` option
- Following rules have been deprecated:
	- scss/at-import-partial-extension-whitelist
	- scss/at-import-partial-extension-blacklist
	- scss/at-import-partial-extension
- Following rules have been added, but turned off:
	- scss/at-import-partial-extension-allowed-list
	- scss/at-import-partial-extension-disallowed-list
	- scss/load-partial-extension
	- scss/at-mixin-no-risky-nesting-selector

## v4.0.1 (2024-01-05):

- Turn off media-query-no-invalid rule for scss

## v4.0.0 (2024-01-04):

- Bump stylelint version to v16.1.0
- Bump stylelint-scss version to v6.0.0
- Bump postcss version to v8.4.32
- Bump postcss-scss version to v4.0.9
- Add @stylistic/stylelint-plugin v2.0.0
- [76 deprecated rules](https://stylelint.io/migration-guide/to-15#deprecated-stylistic-rules) have been removed and use rules from [@stylistic/stylelint-plugin](https://github.com/stylelint-stylistic/stylelint-stylistic) with `@stylistic/` prefix instead
- Following rules have been enabled:
	- declaration-block-no-duplicate-custom-properties
	- declaration-property-value-no-unknown
	- media-feature-name-value-no-unknown
	- media-query-no-invalid
	- selector-anb-no-unmatchable
	- scss/at-use-no-redundant-alias
	- scss/at-root-no-redundant
	- scss/function-calculation-no-interpolation
	- scss/load-no-partial-leading-underscore
	- scss/property-no-unknown
- Following rules have been changed:
	- color-function-notation change option from legacy to modern
- Following rules have been added, but turned off:
	- no-unknown-custom-properties
	- scss/block-no-redundant-nesting
	- scss/function-disallowed-list

## v3.0.0 (2023-01-25):

- Bump stylelint version to v14.16.1
- Bump stylelint-scss version to v4.3.0
- Bump postcss version to v8.4.21
- Bump postcss-scss version to v4.0.6
- Update the config to be able to extend separate lint rules:
	- `"extends": "stylelint-config-maqe"` to extend CSS rules
	- `"extends": "stylelint-config-maqe/scss"` to extend CSS and SCSS rules
- Following rules have been enabled:
	- media-feature-range-notation

## v2.1.0 (2022-08-15):

- Bump stylelint version to v14.10.1
- Following rules have been enabled:
	- keyframe-block-no-duplicate-selectors
	- selector-not-notation
- Following rules have been added, but turned off:
	- declaration-property-max-values
	- function-no-unknown
	- import-notation
- Following rule have been added secondary option:
	- value-keyword-case add camelCaseSvgKeywords option and set true

## v2.0.0 (2022-03-16):

- Migrate stylelint version to 14
- Bump stylelint dependency to >= 14.1.0
- Add new dependencies
	- postcss ^8.4.6
	- postcss-scss ^4.0.3
	- stylelint-scss ^4.1.0
- Following rule have been removed
	- function-calc-no-invalid
- Following rule have been enabled:
	- custom-property-no-missing-var-function
- Add stylelint-scss plugin and following rules have been enabled:
	- scss/at-else-if-parentheses-space-before
	- scss/at-use-no-unnamespaced
	- scss/at-function-parentheses-space-before
	- scss/at-import-no-partial-leading-underscore
	- scss/at-import-partial-extension
	- scss/at-mixin-parentheses-space-before
	- scss/declaration-nested-properties
	- scss/declaration-nested-properties-no-divided-groups
	- scss/dimension-no-non-numeric-values
	- scss/dollar-variable-colon-newline-after
	- scss/dollar-variable-colon-space-after
	- scss/dollar-variable-colon-space-before
	- scss/dollar-variable-no-missing-interpolation
	- scss/dollar-variable-no-namespaced-assignment
	- scss/double-slash-comment-whitespace-inside
	- scss/function-quote-no-quoted-strings-inside
	- scss/function-unquote-no-unquoted-strings-inside
	- scss/media-feature-value-dollar-variable
	- scss/no-duplicate-dollar-variables
	- scss/no-global-function-names
	- scss/operator-no-unspaced
	- scss/selector-no-redundant-nesting-selector
- Add stylelint-scss plugin and following rules have been added, but turned off:
	- scss/at-else-closing-brace-newline-after
	- scss/at-else-empty-line-before
	- scss/at-extend-no-missing-placeholder
	- scss/at-function-named-arguments
	- scss/at-function-pattern
	- scss/at-if-closing-brace-space-after
	- scss/at-if-no-null
	- scss/at-import-partial-extension-blacklist
	- scss/at-import-partial-extension-whitelist
	- scss/at-mixin-argumentless-call-parentheses
	- scss/at-mixin-named-arguments
	- scss/at-mixin-pattern
	- scss/at-rule-conditional-no-parentheses
	- scss/at-rule-no-unknown
	- scss/at-each-key-value-single-line
	- scss/at-else-closing-brace-space-after
	- scss/dollar-variable-default
	- scss/dollar-variable-empty-line-after
	- scss/dollar-variable-empty-line-before
	- scss/dollar-variable-first-in-block
	- scss/dollar-variable-pattern
	- scss/double-slash-comment-empty-line-before
	- scss/double-slash-comment-inline
	- scss/function-color-relative
	- scss/map-keys-quotes
	- scss/no-dollar-variables
	- scss/operator-no-newline-before
	- scss/partial-no-import
	- scss/percent-placeholder-pattern
	- scss/selector-nest-combinators
	- scss/selector-no-union-class-name

## v1.2.2 (2021-10-07):

- Set `custom-property-empty-line-before` rule to null

## v1.2.1 (2021-06-30):

- Add `use` to `at-rule-no-unknown`

## v1.2.0 (2021-05-17):

- Bump Stylelint version to v13.13.1
- Following rules have been enabled:
	- alpha-value-notation
	- color-function-notation
	- comment-word-disallowed-list
	- max-nesting-depth
	- named-grid-areas-no-invalid
	- no-duplicate-at-import-rules
	- no-invalid-position-at-import-rule
	- no-irregular-whitespace
	- unicode-bom
	- unit-disallowed-list
- Following rules have been deprecated:
	- at-rule-property-requirelist
	- comment-word-blacklist
	- media-feature-name-value-whitelist
	- selector-combinator-blacklist
	- selector-combinator-whitelist
	- selector-pseudo-element-blacklist
	- selector-pseudo-element-whitelist
	- unit-blacklist

## v1.1.0 (2019-04-30):

- Bump Stylelint version to v10.0.1
- Following rules have been enabled:
	- function-calc-no-invalid
	- no-empty-first-line
- Following rules have been added, but turned off:
	- at-rule-property-requirelist
	- keyframes-name-pattern
	- linebreaks
	- media-feature-name-value-whitelist
	- selector-combinator-blacklist
	- selector-combinator-whitelist
	- selector-max-pseudo-class
	- selector-pseudo-element-blacklist
	- selector-pseudo-element-whitelist
- Fix "at-rule-no-unknown" throw an error on SCSS directives

## v1.0.2 (2018-01-29):

- Bump Stylint version to v8.4.0

## v1.0.1 (2017-08-22):

- Fix "at-rule-empty-line-before" so we can group @import together.
- Add Changelog.