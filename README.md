```
MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
MMmhhhhmMMMMMMNhhhhdMMMMMMMNhhhhhNMMMMMMMMMNNdhhhmNMMMMMMdhhhhhhhhhhhhhmMM
MMo.....sNMMMh-....:MMMMMMM+.....+MMMMMMMmo:......-/yNMMM:.............oMM
MMo......:hN+......:MMMMMMs.......yMMMMMs....-::-....:dMM/.....::::::::sMM
MMo....-...//--....:MMMMMh....-....hMMMy....omMMNd/...-NMNo-...oNMMMMMMMMM
MMo....h+---:yd....:MMMMd-...:d-...-mMM/...-MMMMMMd....hMMMy/.../NMMMMMMMM
MMo....mMmmmmMM....:MMMm-....dMh....:NMy....smMMNd/...-NMNs-:+:oNMMMMMMMMM
MMo....mMMMMMMM....:MMN/....-ho/.....+MMo....-/oo/:..:dMM/....:////////sMM
MMo....mMMMMMMM....:MM+....-sNNy-.....sMMdo:.....--/+dMMM:.............oMM
MMdhhhhNMMMMMMMhhhhdMNhhhhhmMMMMNhhhhhhMMMMNmy+:.....:dMMdhhhhhhhhhhhhhdMM
MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMNNho:oNMMMMMMMMMMMMMMMMMMMM
MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM

```

# stylelint-config-maqe

This package provides MAQE's .stylelintrc as an extensible shared config.

---

## Usage

### Using MAQE Boilerplate

If you're using [MAQE Boilerplate](https://bitbucket.org/maqe/mqbp), then you don't have to do anything! MAQE Boilerplate already has this package as its dependency. Sit back and enjoy!

### Using without MAQE Boilerplate

If you don't use [MAQE Boilerplate](https://bitbucket.org/maqe/mqbp), you'll need to do the following:

- Install all the peer dependencies
```
npm install -D stylelint stylelint-scss postcss postcss-scss @stylistic/stylelint-plugin
```

- Install the package
```
npm install -D stylelint-config-maqe
```

- Create your .stylelintrc file and extend it from this config
```json
{
    "extends": "stylelint-config-maqe"
}
```
See [Stylelint Documentation](https://stylelint.io/user-guide/configuration/) for more information.

---

## Override stylelint config

You can modify your .stylelintrc in your project as you want. A common override is:

### Rules

You can override the default rules by adding "rules" in your .stylelintrc

```json
{
    "rules": {
        "at-rule-empty-line-before": null
    }
}
```

See [Configuring Rules](https://stylelint.io/user-guide/configuration/) for more information.

See [Rules](https://stylelint.io/user-guide/rules/) for list of stylelint rules.

---

## Migrating to version 2.0.0
This version contains breaking changes that may affect you.

### Support for Node.js 10 was dropped
You should use the following or higher versions of Node.js
- 12.20.0
- 14.13.1
- 16.0.0

### Required more dependencies when need to lint anything other than CSS or SCSS
If you use stylelint-config-maqe to lint anything other than CSS or SCSS files, you will need to install and configure these syntaxes.
- Less language (.less) use postcss-less
- Sass language (.sass) use postcss-sass
- CSS-in-JS embeds (.js, .jsx, .ts etc.) use @stylelint/postcss-css-in-js
- HTML, XML and HTML-like embeds (.html, .xml, .svelte, .vue etc.) use postcss-html
- Markdown embeds (.md, .markdown etc.) use postcss-markdown

#### Example for Vue.js
You will need to install 
```
npm install -D postcss-html
```
and then overrides config in file .stylelintrc
```json
{
    "overrides": [
        {
            "files": ["*.vue", "**/*.vue"],
            "customSyntax": "postcss-html"
        }
    ]
}
```
Note: If you use NuxtJS with PostCSS version 7 or below, you will get many warning from PostCSS on build time. Migrate to PostCSS version 8 or above to solve this issue.

Example migrate to PostCSS version 8 for NuxtJS
```
npm install -D @nuxt/postcss8 postcss@8
```
and then add config to nuxt.config.js
```js
{
    buildModules: [
	    '@nuxt/postcss8'
    ]
}
```

---

## Migrating to version 3.0.0
### Before version 3.0.0
We only have one configuration for extending `styelint-config-maqe` in addition to `stylelint` rules, our package also automatically adds `stylelint-scss` plugin and sets it up to support many rules such as `scss/at-rule-no-unknown`.
```json
{
    "extends": "stylelint-config-maqe"
}
```

### In version 3.0.0
If you extend the same way as previous versions, our package will only include rules from `stylelint` such as `alpha-value-notation`
```json 
{
    "extends": "stylelint-config-maqe"
}
``` 

If you want to use our config rules from `stylelint-scss` plugin such as `scss/at-rule-no-unknown` you can use the following configuration 
```json
{
    "extends": "stylelint-config-maqe/scss",
    "rules": {
        // override stylelint-scss plugin rules
        "scss/dimension-no-non-numeric-values": true,
    }
}
```

---

## Migration to version 4.0.0
### Removed support for Node.js less than version 18.12.0

### Removed stylistic rules from stylelint and use `@stylistic/stylelint-plugin` plugin instead
[Stylelint has removed 76 rules](https://stylelint.io/migration-guide/to-16/#removed-deprecated-stylistic-rules) pertaining to stylistic conventions. We employ the [@stylistic/stylelint-plugin](https://github.com/stylelint-stylistic/stylelint-stylistic?tab=readme-ov-file) for maintaining stylistic linting, and you can modify specific rules by prefixed with `@stylistic/`.

Install a new dependency package 
```
npm i -D @stylistic/stylelint-plugin
```
```json
{
    "extends": "stylelint-config-maqe",
    "rules": {
        // stylistic rules from @stylistic/stylelint-plugin:
		"@stylistic/color-hex-case": "lower",
    }
}
```